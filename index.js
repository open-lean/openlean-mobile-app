import * as React from "react";
import {Navigation} from "navigation-rnn";
// import {Navigation} from "navigation-rn";
import {AppRegistry, Button, Text, View} from 'react-native';
import {name as appName} from './app.json';

/*
Switch between react-native-navigation or @react-navigation. This need change in 5 files:
1. File index.js: change import from "navigation-rnn" or "navigation-rn"
2. File package.json: add oer remove "react-native-navigation": "*",
3. File MainApplication.java: switch between option 1 and option 2
4. File MainAActivity.java: switch between option 1 and option 2
5. [Optional] File android/build.gradle: add or remove 3 lines as comment
 */

// Screens: [[name, Screen, optionsForRN]]
const screens = [
    ['Login', LoginScreen],
    ['Home', HomeScreen],
];

// Init navigation
if (Navigation.type === 'RNN') {
    Navigation.registerComponents(screens);
    Navigation.events().registerAppLaunchedListener(() => {
        Navigation.setDefaultOptions({
            topBar: {
                visible: false,
            }
        });

        Navigation.setRoot({
            root: {
                stack: {
                    children: [
                        {
                            component: {
                                name: 'Login',
                                passProps: {nav: Navigation},
                            }
                        }
                    ]
                }
            }
        });
    });
} else if (Navigation.type === 'RN') {
    const defaultOptions = {headerShown: false};
    Navigation.registerComponents(screens, defaultOptions);
    AppRegistry.registerComponent(appName, () => Navigation.App);
}

// Export for the functions call to show/hide a screen.
export {Navigation};

// Components example
function LoginScreen({nav}) {
    return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Text>Login Screen</Text>
            <Button onPress={() => nav?.showModal?.('Home')} title={'Open Modal Home'}/>
            <Button onPress={() => nav?.showOverlay?.('Home')} title={'Open Overlay Home'}/>
        </View>
    );
}

function HomeScreen({componentId, nav}) {
    return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Text>Home Screen</Text>
            <Button onPress={() => nav?.showModal?.('Home')} title={'Open Modal Home'}/>
            <Button onPress={() => nav?.dismissModal?.(componentId)} title={'Close Modal'}/>
            <Button onPress={() => nav?.dismissOverlay?.(componentId)} title={'Close Overlay'}/>
        </View>
    );
}
