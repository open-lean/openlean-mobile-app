## OpenLean Mobile Application

### Install
- `npm ci` to install the packages in package-lock.json
- iOS: `cd ios` and `pod install --deployment` to pod install the packages in Podfile.lock
- Init a new workspace as `npm init -w ./packages/navigation-rnn`
- Install a package to a workspace as `npm install react-native-navigation@7.32.1 -w ./packages/navigation-rnn`
- React Native Navigation:
  - Read more: https://wix.github.io/react-native-navigation/
  - `npm install react-native-navigation -w ./packages/navigation-rnn`
- React Navigation:
  - Read more: https://reactnative.dev/docs/navigation
  - `npm install @react-navigation/native @react-navigation/native-stack react-native-safe-area-context react-native-screens -w ./packages/navigation-rn`
- In root/package.json:
  - Use react-native-navigation then add `"react-native-navigation": "*",` to dependencies to link automatic
  - Use @react-navigation then add `"react-native-safe-area-context": "*", "react-native-screens": "*",` to dependencies to link automatic

### Run with development mode
- Read more: https://reactnative.dev/docs/debugging
- Android: `npm run android`
- iOS: `npm run ios`
