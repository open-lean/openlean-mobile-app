import {Navigation} from "react-native-navigation";

/*
OLNavigation re-define a navigation base react-native-navigation with:
- showModal, dismissModal: show and dismiss a modal
- showOverlay, dismissOverlay: show and dismiss an overlay
 */
class OLNavigation {
    static type = 'RNN';
    static events = () => Navigation.events();
    static setRoot = (props) => Navigation.setRoot(props);
    static setDefaultOptions = (props) => Navigation.setDefaultOptions(props);
    static registerComponents = (components = []) => {
        components.map(([screenName, ScreenComponent]) => Navigation.registerComponent(screenName, () => ScreenComponent));
    };

    // Return native navigation of RNN
    static get navRNN() {
        return Navigation;
    };

    /**
     * Show a new screen as a modal.
     * On Android: can use hardware back button to dismiss a model.
     * @param name
     * @param passProps
     * @param options
     */
    static showModal = (name, passProps = {}, options = {}) => {
        Navigation.showModal({
            stack: {
                children: [{
                    component: {
                        name,
                        passProps: {...passProps, nav: OLNavigation},
                        options: {
                            modalPresentationStyle: 'overCurrentContext',
                            layout: {
                                backgroundColor: 'transparent'
                            },
                            ...options
                        }
                    }
                }]
            }
        }).then();
    };

    /**
     * Dismiss a modal.
     * @param componentId
     * @param hasAnimation
     * Error: Pixel crash if (dismissAnimation==true and the screen has WebView)
     */
    static dismissModal = (componentId = '', hasAnimation = true) => {
        // OLNavigation.allowHandleBackPress &&
        Navigation.dismissModal(componentId,
            {
                animations: {
                    dismissModal: {
                        enable: hasAnimation
                    }
                }
            }
        ).then();
    };

    /**
     * Show a screen as an overlay.
     * @param name
     * @param passProps
     * @param options
     */
    static showOverlay = (name, passProps = {}, options = {}) => {
        Navigation.showOverlay({
            component: {
                name,
                passProps: {...passProps, nav: OLNavigation},
                options: {
                    layout: {
                        componentBackgroundColor: 'transparent'
                    },
                    overlay: {
                        interceptTouchOutside: true
                    },
                    ...options
                }
            }
        }).then();
    };

    /**
     * Dismiss an overlay.
     * @param componentId
     */
    static dismissOverlay = (componentId = '') => {
        Navigation.dismissOverlay(componentId).then();
    };
}

export {OLNavigation as Navigation};
