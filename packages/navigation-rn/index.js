import React, {useRef} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

/**
 * https://reactnative.dev/docs/navigation
 * https://reactnavigation.org/docs/getting-started/
 */

const Stack = createNativeStackNavigator();
let navigationRef = undefined;

const AppNavigationContainer = ({components, defaultOptions}) => {
    navigationRef = useRef();
    return (
        <NavigationContainer ref={navigationRef}>
            <Stack.Navigator>
                {components.map(([screenName, ScreenComponent, options = {}]) => <Stack.Screen
                    key={screenName}
                    name={screenName}
                    options={{...defaultOptions, ...options}}
                >
                    {
                        (props) => <ComponentExtToAdjustOptions
                            {...props}
                        >
                            <ScreenComponent
                                {...props} // have navigation, route
                                {...props.route.params} // use params as the native props of ScreenComponent
                                nav={OLNavigation} // use nav instead navigation, use to it same in react-native-navigation
                                componentId={props.route.key} // this is good when switch to react-native-navigation
                            />
                        </ComponentExtToAdjustOptions>
                    }
                </Stack.Screen>)}
            </Stack.Navigator>
        </NavigationContainer>
    );
}

// This component to use navigation?.setOptions with case: showModal, showOverlay.
const ComponentExtToAdjustOptions = ({children, navigation, route}) => {
    React.useEffect(() => {
        navigation?.setOptions?.(route?.params?.options);
    }, [navigation]);
    return <>
        {children}
    </>;
}

class OLNavigation {
    static type = 'RN';
    static components = [];
    static defaultOptions = {};
    // Return native navigation of RN
    static get navRN() {
        return navigationRef?.current;
    };
    // Register all screen and options default.
    static registerComponents = (components = [], defaultOptions = {}) => {
        OLNavigation.components = components;
        OLNavigation.defaultOptions = defaultOptions;
    };
    // Return a component, it is used as App component.
    static App = () => <AppNavigationContainer components={OLNavigation.components} defaultOptions={OLNavigation.defaultOptions}/>;

    /**
     * Show a modal
     * @param name
     * @param passProps
     * @param options
     */
    static showModal = (name, passProps = {}, options = {}) => {
        // addListener
        // canGoBack
        // dispatch
        // getCurrentOptions
        // getCurrentRoute
        // getParent
        // getRootState
        // getState
        // goBack
        // isFocused
        // isReady
        // navigate
        // removeListener
        // reset
        // resetRoot
        // setParams
        navigationRef?.current?.navigate({
            name,
            key: Date.now().toString(),
            params: {...passProps, options: {presentation: 'transparentModal', headerShown: false, cardOverlayEnabled: false, ...options}}
        });
    };

    /**
     * Dismiss a modal, it is re-build from @react-navigation.
     */
    static dismissModal = (componentId) => {
        componentId && // this is good when switch to react-native-navigation
        navigationRef?.current?.canGoBack() &&
        navigationRef?.current?.goBack();
    };

    /**
     * Show a new screen as an overlay.
     * @param name
     * @param passProps
     * @param options
     */
    static showOverlay = (name, passProps = {}, options = {}) => {
        navigationRef?.current?.navigate({
            name,
            key: Date.now().toString(),
            params: {...passProps, options: {presentation: 'transparentModal', headerShown: false, cardOverlayEnabled: true, animationEnabled: false, ...options}}
        });
    };

    /**
     * Dismiss an overlay, it is re-built from @react-navigation.
     */
    static dismissOverlay = (componentId) => {
        componentId && // this is good when switch to react-native-navigation
        navigationRef?.current?.canGoBack() &&
        navigationRef?.current?.goBack();
    };
}

export {OLNavigation as Navigation};
